" vim: filetype=vim ts=2 sw=2 et

hi clear

if exists("syntax_on")
    syntax reset
endif
"set background=light
let colors_name = "minimal"

hi DiffText cterm=bold ctermbg=Red 
hi DiffAdd ctermbg=yellow 
hi DiffChange ctermbg=yellow 
hi DiffDelete ctermbg=yellow 
hi Cursor ctermfg=red
hi Constant ctermfg=6
hi Identifier ctermfg=4
hi CursorLineNr ctermfg=243 ctermbg=255
hi ColorColumn ctermbg=255
"
"
if &background == "light"

hi Normal ctermfg=black ctermbg=white
hi Comment cterm=none ctermfg=150 
hi LineNr ctermfg=238 ctermbg=none
hi SignColumn ctermfg=250 ctermbg=254
hi Folded ctermfg=250 ctermbg=white
hi FoldColumn ctermfg=250 ctermbg=255


else

" Dark Mode
hi Normal ctermfg=7
hi String cterm=bold ctermfg=167 
hi Comment cterm=none ctermfg=217 
hi LineNr ctermfg=240 ctermbg=236
hi SignColumn ctermfg=240 ctermbg=236
hi FoldColumn ctermfg=240 ctermbg=236
hi Folded ctermfg=217 ctermbg=none
hi FgCocWarningFloatBgCocFloating cterm=bold ctermfg=231 ctermbg=160 
hi FgCocErrorFloatBgCocFloating cterm=bold ctermfg=231 ctermbg=160 
hi Pmenu cterm=bold ctermfg=235 ctermbg=217
hi PmenuSel cterm=bold ctermfg=yellow ctermbg=red
hi PreProc cterm=bold ctermfg=66
endif

