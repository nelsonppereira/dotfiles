" vim: filetype=vim ts=2 sw=2 et

hi clear

if exists("syntax_on")
  syntax reset
endif

set background=dark
let colors_name="vscode"
set termguicolors

let s:keepcpo= &cpo
set cpo&vim

call extend(v:colornames, {
      \ 'vscfront': '#d4d4d4',
      \ 'vscback': '#1e1e1e',
      \ 'vsctabcurrent': '#1e1e1e',
      \ 'vsctabother': '#2d2d2d',
      \ 'vsctaboutside': '#252526',
      \ 'vscleftdark': '#252526',
      \ 'vscleftmid': '#373737',
      \ 'vscleftlight': '#636369',
      \ 'vscpopupfront': '#bbbbbb',
      \ 'vscpopupback': '#272727',
      \ 'vscpopuphighlightblue': '#004b72',
      \ 'vscpopuphighlightgray': '#343b41',
      \ 'vscsplitlight': '#898989',
      \ 'vscsplitdark': '#444444',
      \ 'vscsplitthumb': '#424242',
      \ 'vsccursordarkdark': '#222222',
      \ 'vsccursordark': '#51504f',
      \ 'vsccursorlight': '#aeafad',
      \ 'vscselection': '#264f78',
      \ 'vsclinenumber': '#5a5a5a',
      \ 'vscdiffreddark': '#4b1818',
      \ 'vscdiffredlight': '#6f1313',
      \ 'vscdiffredlightlight': '#fb0101',
      \ 'vscdiffgreendark': '#373d29',
      \ 'vscdiffgreenlight': '#4b5632',
      \ 'vscsearchcurrent': '#515c6a',
      \ 'vscsearch': '#613315',
      \ 'vscgitadded': '#81b88b',
      \ 'vscgitmodified': '#e2c08d',
      \ 'vscgitdeleted': '#c74e39',
      \ 'vscgitrenamed': '#73c991',
      \ 'vscgituntracked': '#73c991',
      \ 'vscgitignored': '#8c8c8c',
      \ 'vscgitstagemodified': '#e2c08d',
      \ 'vscgitstagedeleted': '#c74e39',
      \ 'vscgitconflicting': '#e4676b',
      \ 'vscgitsubmodule': '#8db9e2',
      \ 'vsccontext': '#404040',
      \ 'vsccontextcurrent': '#707070',
      \ 'vscfoldbackground': '#202d39',
      \ 'vscgray': '#808080',
      \ 'vscviolet': '#646695',
      \ 'vscblue': '#569cd6',
      \ 'vscaccentblue': '#4fc1fe',
      \ 'vscdarkblue': '#223e55',
      \ 'vscmediumblue': '#18a2fe',
      \ 'vsclightblue': '#9cdcfe',
      \ 'vscgreen': '#6a9955',
      \ 'vscbluegreen': '#4ec9b0',
      \ 'vsclightgreen': '#b5cea8',
      \ 'vscred': '#f44747',
      \ 'vscorange': '#ce9178',
      \ 'vsclightred': '#d16969',
      \ 'vscyelloworange': '#d7ba7d',
      \ 'vscyellow': '#dcdcaa',
      \ 'vscdarkyellow': '#ffd602',
      \ 'vscpink': '#c586c0',
      \ 'vscdimhighlight':'#51504f',
      \}, 'keep' )

    hi Normal guifg=vscfront guibg=vscback
    hi ColorColumn guifg=NONE guibg=vsccursordarkdark 
    hi Cursor guifg=vsccursordark guibg=vsccursorlight 
    hi CursorLine  guibg=vsccursordarkdark 
    hi CursorColumn guifg=NONE guibg=vsccursordarkdark 
    hi Directory guifg=vscblue guibg=vscback 
    hi DiffAdd guifg=NONE guibg=vscdiffgreenlight 
    hi DiffChange guifg=NONE guibg=vscdiffreddark 
    hi DiffDelete guifg=NONE guibg=vscdiffredlight 
    hi DiffText guifg=NONE guibg=vscdiffredlight 
    hi EndOfBuffer guifg=vscback guibg=NONE 
    hi ErrorMsg guifg=vscred guibg=vscback 
    hi VertSplit guifg=vscsplitdark guibg=vscback 
    hi link WinSeparator VertSplit 
    hi Folded guifg=NONE guibg=vscfoldbackground 
    hi FoldColumn guifg=vsclinenumber guibg=vscback 
    hi SignColumn guifg=NONE guibg=vscback 
    hi IncSearch guifg=NONE guibg=vscsearchcurrent 
    hi LineNr guifg=vsclinenumber guibg=vscback 
    hi CursorLineNr guifg=vscpopupfront guibg=vscback 
    hi MatchParen guifg=NONE guibg=vscdimhighlight 
    hi ModeMsg guifg=vscfront guibg=vscleftdark 
    hi MoreMsg guifg=vscfront guibg=vscleftdark 
    hi NonText guifg=vsclinenumber
    hi Pmenu guifg=vscpopupfront guibg=vscpopupback 
    hi PmenuSel guifg=vscpopupfront
    hi PmenuSbar guifg=NONE guibg=vscpopuphighlightgray 
    hi PmenuThumb guifg=NONE guibg=vscpopupfront 
    hi Question guifg=vscblue guibg=vscback 
    hi Search guifg=NONE guibg=vscsearch 
    hi SpecialKey guifg=vscblue guibg=NONE 
    hi StatusLine guifg=vscfront guibg=vscleftmid 
    hi TabLine guifg=vscfront guibg=vsctabother 
    hi TabLineFill guifg=vscfront guibg=vsctaboutside 
    hi TabLineSel guifg=vscfront guibg=vsctabcurrent 
    hi Title guifg=NONE guibg=NONE cterm=bold 
    hi Visual guifg=NONE guibg=vscselection 
    hi VisualNOS guifg=NONE guibg=vscselection 
    hi WarningMsg guifg=vscred guibg=vscback cterm=bold
    hi WildMenu guifg=NONE guibg=vscselection 
    hi Comment guifg=vscgreen guibg=NONE cterm=italic
    hi Constant guifg=vscblue guibg=NONE 
    hi String guifg=vscorange guibg=NONE 
    hi Character guifg=vscorange guibg=NONE 
    hi Number guifg=vsclightgreen guibg=NONE 
    hi Boolean guifg=vscblue guibg=NONE 
    hi Float guifg=vsclightgreen guibg=NONE 
    hi Identifier guifg=vsclightblue guibg=NONE 
    hi Function guifg=vscyellow guibg=NONE 
    hi Statement guifg=vscpink guibg=NONE 
    hi Conditional guifg=vscpink guibg=NONE 
    hi Repeat guifg=vscpink guibg=NONE 
    hi Label guifg=vscpink guibg=NONE 
    hi Operator guifg=vscfront guibg=NONE 
    hi Keyword guifg=vscpink guibg=NONE 
    hi Exception guifg=vscpink guibg=NONE 
    hi PreProc guifg=vscpink guibg=NONE 
    hi Include guifg=vscpink guibg=NONE 
    hi Define guifg=vscpink guibg=NONE 
    hi Macro guifg=vscpink guibg=NONE 
    hi Type guifg=vscblue guibg=NONE 
    hi StorageClass guifg=vscblue guibg=NONE 
    hi Structure guifg=vscbluegreen guibg=NONE 
    hi Typedef guifg=vscblue guibg=NONE 
    hi Special guifg=vscyelloworange guibg=NONE 
    hi SpecialChar guifg=vscfront guibg=NONE 
    hi Tag guifg=vscfront guibg=NONE 
    hi Delimiter guifg=vscfront guibg=NONE 
    hi SpecialComment guifg=vscgreen guibg=NONE 
    hi Debug guifg=vscfront guibg=NONE 
    hi Underlined guifg=NONE guibg=NONE cterm=underline
    hi Conceal guifg=vscfront guibg=vscback 
    hi Ignore guifg=vscfront guibg=NONE 
    hi Error guifg=vscred guibg=vscback cterm=undercurl guisp=vscred 
    hi Todo guifg=vscyelloworange guibg=vscback cterm=bold 
    hi SpellBad guifg=vscred guibg=vscback cterm=undercurl guisp=vscred 
    hi SpellCap guifg=vscred guibg=vscback cterm=undercurl guisp=vscred 
    hi SpellRare guifg=vscred guibg=vscback cterm=undercurl  guisp=vscred 
    hi SpellLocal guifg=vscred guibg=vscback cterm=undercurl guisp=vscred 
    hi Whitespace guifg=vsclinenumber
    hi NormalFloat  guibg=vscpopupback 
    hi WinBar guifg=vscfront guibg=vscback cterm=bold 
    hi WinBarNc guifg=vscfront guibg=vscback 


    " -- COnvim
    hi CocHighlightText guifg=NONE guibg=vscpopuphighlightgray
    hi CocHighlightRead guifg=NONE guibg=vscpopuphighlightgray
    hi CocHighlightWrite guifg=NONE guibg=vscpopuphighlightgray

" -- Python
    hi pythonStatement guifg=vscblue guibg=NONE 
    hi pythonOperator guifg=vscblue guibg=NONE 
    hi pythonException guifg=vscpink guibg=NONE 
    hi pythonExClass guifg=vscbluegreen guibg=NONE 
    hi pythonBuiltinObj guifg=vsclightblue guibg=NONE 
    hi pythonBuiltinType guifg=vscbluegreen guibg=NONE 
    hi pythonBoolean guifg=vscblue guibg=NONE 
    hi pythonNone guifg=vscblue guibg=NONE 
    hi pythonTodo guifg=vscblue guibg=NONE 
    hi pythonClassVar guifg=vscblue guibg=NONE 
    hi pythonClassDef guifg=vscbluegreen guibg=NONE 

    " -- JSON
    hi jsonKeyword guifg=vsclightblue guibg=NONE 
    hi jsonEscape guifg=vscyelloworange guibg=NONE 
    hi jsonNull guifg=vscblue guibg=NONE 
    hi jsonBoolean guifg=vscblue guibg=NONE 

    " -- HTML
    hi htmlTag guifg=vscgray guibg=NONE 
    hi htmlEndTag guifg=vscgray guibg=NONE 
    hi htmlTagName guifg=vscblue guibg=NONE 
    hi htmlSpecialTagName guifg=vscblue guibg=NONE 
    hi htmlArg guifg=vsclightblue guibg=NONE 

    " -- PHP
    hi phpStaticClasses guifg=vscbluegreen guibg=NONE 
    hi phpMethod guifg=vscyellow guibg=NONE 
    hi phpClass guifg=vscbluegreen guibg=NONE 
    hi phpFunction guifg=vscyellow guibg=NONE 
    hi phpInclude guifg=vscblue guibg=NONE 
    hi phpUseClass guifg=vscbluegreen guibg=NONE 
    hi phpRegion guifg=vscbluegreen guibg=NONE 
    hi phpMethodsVar guifg=vsclightblue guibg=NONE 

    " -- CSS
    hi cssBraces guifg=vscfront guibg=NONE 
    hi cssInclude guifg=vscpink guibg=NONE 
    hi cssTagName guifg=vscyelloworange guibg=NONE 
    hi cssClassName guifg=vscyelloworange guibg=NONE 
    hi cssPseudoClass guifg=vscyelloworange guibg=NONE 
    hi cssPseudoClassId guifg=vscyelloworange guibg=NONE 
    hi cssPseudoClassLang guifg=vscyelloworange guibg=NONE 
    hi cssIdentifier guifg=vscyelloworange guibg=NONE 
    hi cssProp guifg=vsclightblue guibg=NONE 
    hi cssDefinition guifg=vsclightblue guibg=NONE 
    hi cssAttr guifg=vscorange guibg=NONE 
    hi cssAttrRegion guifg=vscorange guibg=NONE 
    hi cssColor guifg=vscorange guibg=NONE 
    hi cssFunction guifg=vscorange guibg=NONE 
    hi cssFunctionName guifg=vscorange guibg=NONE 
    hi cssVendor guifg=vscorange guibg=NONE 
    hi cssValueNumber guifg=vscorange guibg=NONE 
    hi cssValueLength guifg=vscorange guibg=NONE 
    hi cssUnitDecorators guifg=vscorange guibg=NONE 
    hi cssStyle guifg=vsclightblue guibg=NONE 
    hi cssImportant guifg=vscblue guibg=NONE 

    " -- JavaScript
    hi jsVariableDef guifg=vsclightblue guibg=NONE 
    hi jsFuncArgs guifg=vsclightblue guibg=NONE 
    hi jsFuncBlock guifg=vsclightblue guibg=NONE 
    hi jsRegexpString guifg=vsclightred guibg=NONE 
    hi jsThis guifg=vscblue guibg=NONE 
    hi jsOperatorKeyword guifg=vscblue guibg=NONE 
    hi jsDestructuringBlock guifg=vsclightblue guibg=NONE 
    hi jsObjectKey guifg=vsclightblue guibg=NONE 
    hi jsGlobalObjects guifg=vscbluegreen guibg=NONE 
    hi jsModuleKeyword guifg=vsclightblue guibg=NONE 
    hi jsClassDefinition guifg=vscbluegreen guibg=NONE 
    hi jsClassKeyword guifg=vscblue guibg=NONE 
    hi jsExtendsKeyword guifg=vscblue guibg=NONE 
    hi jsExportDefault guifg=vscpink guibg=NONE 
    hi jsFuncCall guifg=vscyellow guibg=NONE 
    hi jsObjectValue guifg=vsclightblue guibg=NONE 
    hi jsParen guifg=vsclightblue guibg=NONE 
    hi jsObjectProp guifg=vsclightblue guibg=NONE 
    hi jsIfElseBlock guifg=vsclightblue guibg=NONE 
    hi jsParenIfElse guifg=vsclightblue guibg=NONE 
    hi jsSpreadOperator guifg=vsclightblue guibg=NONE 
    hi jsSpreadExpression guifg=vsclightblue guibg=NONE 

    " -- Typescript
    hi typescriptLabel guifg=vsclightblue guibg=NONE 
    hi typescriptExceptions guifg=vsclightblue guibg=NONE 
    hi typescriptBraces guifg=vscfront guibg=NONE 
    hi typescriptEndColons guifg=vsclightblue guibg=NONE 
    hi typescriptParens guifg=vscfront guibg=NONE 
    hi typescriptDocTags guifg=vscblue guibg=NONE 
    hi typescriptDocComment guifg=vscbluegreen guibg=NONE 
    hi typescriptLogicSymbols guifg=vsclightblue guibg=NONE 
    hi typescriptImport guifg=vscpink guibg=NONE 
    hi typescriptBOM guifg=vsclightblue guibg=NONE 
    hi typescriptVariableDeclaration guifg=vsclightblue guibg=NONE 
    hi typescriptVariable guifg=vscblue guibg=NONE 
    hi typescriptExport guifg=vscpink guibg=NONE 
    hi typescriptAliasDeclaration guifg=vscbluegreen guibg=NONE 
    hi typescriptAliasKeyword guifg=vscblue guibg=NONE 
    hi typescriptClassName guifg=vscbluegreen guibg=NONE 
    hi typescriptAccessibilityModifier guifg=vscblue guibg=NONE 
    hi typescriptOperator guifg=vscblue guibg=NONE 
    hi typescriptArrowFunc guifg=vscblue guibg=NONE 
    hi typescriptMethodAccessor guifg=vscblue guibg=NONE 
    hi typescriptMember guifg=vscyellow guibg=NONE 
    hi typescriptTypeReference guifg=vscbluegreen guibg=NONE 
    hi typescriptTemplateSB guifg=vscyelloworange guibg=NONE 
    hi typescriptArrowFuncArg guifg=vsclightblue guibg=NONE 
    hi typescriptParamImpl guifg=vsclightblue guibg=NONE 
    hi typescriptFuncComma guifg=vsclightblue guibg=NONE 
    hi typescriptCastKeyword guifg=vsclightblue guibg=NONE 
    hi typescriptCall guifg=vscblue guibg=NONE 
    hi typescriptCase guifg=vsclightblue guibg=NONE 
    hi typescriptReserved guifg=vscpink guibg=NONE 
    hi typescriptDefault guifg=vsclightblue guibg=NONE 
    hi typescriptDecorator guifg=vscyellow guibg=NONE 
    hi typescriptPredefinedType guifg=vscbluegreen guibg=NONE 
    hi typescriptClassHeritage guifg=vscbluegreen guibg=NONE 
    hi typescriptClassExtends guifg=vscblue guibg=NONE 
    hi typescriptClassKeyword guifg=vscblue guibg=NONE 
    hi typescriptBlock guifg=vsclightblue guibg=NONE 
    hi typescriptDOMDocProp guifg=vsclightblue guibg=NONE 
    hi typescriptTemplateSubstitution guifg=vsclightblue guibg=NONE 
    hi typescriptClassBlock guifg=vsclightblue guibg=NONE 
    hi typescriptFuncCallArg guifg=vsclightblue guibg=NONE 
    hi typescriptIndexExpr guifg=vsclightblue guibg=NONE 
    hi typescriptConditionalParen guifg=vsclightblue guibg=NONE 
    hi typescriptArray guifg=vscyellow guibg=NONE 
    hi typescriptES6SetProp guifg=vsclightblue guibg=NONE 
    hi typescriptObjectLiteral guifg=vsclightblue guibg=NONE 
    hi typescriptTypeParameter guifg=vscbluegreen guibg=NONE 
    hi typescriptEnumKeyword guifg=vscblue guibg=NONE 
    hi typescriptEnum guifg=vscbluegreen guibg=NONE 
    hi typescriptLoopParen guifg=vsclightblue guibg=NONE 
    hi typescriptParenExp guifg=vsclightblue guibg=NONE 
    hi typescriptModule guifg=vsclightblue guibg=NONE 
    hi typescriptAmbientDeclaration guifg=vscblue guibg=NONE 
    hi typescriptFuncTypeArrow guifg=vscblue guibg=NONE 
    hi typescriptInterfaceHeritage guifg=vscbluegreen guibg=NONE 
    hi typescriptInterfaceName guifg=vscbluegreen guibg=NONE 
    hi typescriptInterfaceKeyword guifg=vscblue guibg=NONE 
    hi typescriptInterfaceExtends guifg=vscblue guibg=NONE 
    hi typescriptGlobal guifg=vscbluegreen guibg=NONE 
    hi typescriptAsyncFuncKeyword guifg=vscblue guibg=NONE 
    hi typescriptFuncKeyword guifg=vscblue guibg=NONE 
    hi typescriptGlobalMethod guifg=vscyellow guibg=NONE 
    hi typescriptPromiseMethod guifg=vscyellow guibg=NONE 

    " -- XML
    hi xmlTag guifg=vscblue guibg=NONE 
    hi xmlTagName guifg=vscblue guibg=NONE 
    hi xmlEndTag guifg=vscblue guibg=NONE 



