" vim: filetype=vim ts=2 sw=2 et
"
"       autoindent:  "off" as I usually do not write code.
set   autoindent
"
"       autowrite: "on" saves a lot of trouble
set   autowrite
"
"       backup:  backups are for wimps  ;-)
set   nobackup
"
"       backspace:  '2' is much smarter.
set   backspace=2
"
"
set noswapfile

"       compatible, we are using VIM....
set   nocompatible
"
"       errorbells: damn this beep!  ;-)
set   noerrorbells

"       esckeys:    allow usage of cursor keys within insert mode
set   esckeys
"
"       expandtab:  use spaces instead of tabs
set   expandtab
"
"       foldmethod:  syntax is good, but sometimes can be annoying
"set   foldmethod=syntax
"
"       helpheight: zero disables this.
set   helpheight=0
"
"       hidden:
set   hidden
"
"       hlsearch :  highlight search - show the current search pattern
"       This is a nice feature sometimes - but it sure can get in the
"       way sometimes when you edit.
set   hlsearch
"
"
"       ignorecase:  ignore the case in search patterns?  NO!
set   ignorecase
"
"       incsearch:  shows the matches as you type
set   incsearch

"       insertmode:  start in insert mode?  Naah.
set   noinsertmode
"
"
"       laststatus:  show status line?  Yes, always!
"       laststatus:  Even for only one buffer.
set   laststatus=2
"
" [VIM5]lazyredraw:  do not update screen while executing macros
set   lazyredraw
"
"       magic:       use some magic in search patterns?  Certainly!
set   magic
"
"       modeline:    ...
"       Allow the last line to be a modeline - useful when
"       the last line in sig gives the preferred textwidth for replies.
set   modeline
set   modelines=1
"
"       number:  Show Line numbers.  Together with relativenumber, provides hybrid.
set   number
set   relativenumber

"
"       path:   The list of directories to search when you specify
"               a file with an edit command.
"               Note:  "~/.P" is a symlink to my dir with www pages
"               "$VIM/syntax" is where the syntax files are.  See wildignore for exclusions
"
set   path+=**
"
"       report: show a report when N lines were changed.
"               report=0 thus means "show all changes"!
set   report=0
"
"       ruler:       show cursor position?  Yep!
set   ruler
"
"       shiftwidth:  Number of spaces to use for each
"                    insertion of (auto)indent.
set   shiftwidth=4
"
"       shortmess:   Kind of messages to show.   Abbreviate them all!
"          New since vim-5.0v: flag 'I' to suppress "intro message".
set   shortmess=atc
"
"       showcmd:     Show current uncompleted command?  Absolutely!
set   showcmd
"
"       showmatch:   Show the matching bracket for the last ')'?
set   showmatch
"
"       showmode:    Show the current mode?  YEEEEEEEEESSSSSSSSSSS!
set   showmode
"
"       smartcase:   Ignore the case when searching unless case is entered on the search string
set   smartcase
"
"       suffixes: Suffixes to ignore for file completion
set   suffixes=.bak,~,.o,.so,.swp,.obj
"
"       tabstop
set   tabstop=4
"
"
"       tags
set   tags=./tags,tags,../tags,../../tags
"
"       term
" set   term=rxvt
"
"       textwidth
set   textwidth=0
"
"       title:
set   notitle
"
"       ttyfast:     are we using a fast terminal?
"                    seting depends on where I use Vim...
"set   nottyfast
"
"       ttybuiltin:
"set   nottybuiltin
"
"       ttyscroll:      turn off scrolling -> faster!
"set   ttyscroll=0
"
"       ttytype:
" set   ttytype=rxvt
"
"       viminfo:  What info to store from an editing session
"                 in the viminfo file;  can be used at next session.
set   viminfo=%,'50,\"100,:100,n~/.viminfo
"
"       visualbell:
set   novisualbell
"
"       t_vb:  terminal's visual bell - turned off to make Vim quiet!
set   t_vb=
"
"       whichwrap:
set   whichwrap=<,>
"
"       wildchar  the char used for "expansion" on the command line
"                 default value is "<C-E>" but I prefer the tab key:
set   wildchar=<TAB>

"       wildignore: paths that are ignored during file completion
"set wildignore=
"
"       wildmenu: tab completion
set   wildmenu
"
"       wildmode: list:longest, shows all available choices and filters out as you type
set   wildmode=list:longest
"
"       wrapmargin:
set   wrapmargin=0
"
"       writebackup:
set   nowritebackup
"
" ===================================================================
" Build stuff
" ===================================================================
"
"       makeprg:
"set makeprg=
"
"       errorformat:
"set errorformat =%Dmake[%*\\d]:\ Entering\ directory\ `%f'
"set errorformat+=%f:%l:%c:\ %trror:\ %m
"
" ===================================================================
" Indenting stuff
" ===================================================================
"
"       cindent:
"set    nocindent
"
"       cinoptions:
set     cinoptions=g0,t0,(0

" ===================================================================
" Customizing the command line
" ===================================================================
" Valid names for keys are:  <Up> <Down> <Left> <Right> <Home> <End>
" <S-Left> <S-Right> <S-Up> <PageUp> <S-Down> <PageDown>  <LeftMouse>
"
" Many shells allow editing in "Emacs Style".
" Although I love Vi, I am quite used to this kind of editing now.
" So here it is - commdand line editing commands in emacs style:
"
"


