# USAGE
#
# make           - compile
# make upload    - upload to arduino
# make monitor   - serial monitor

include $(ARDMK_DIR)/Arduino.mk

# Addition libs to include from $(ARDMK_DIR)/libraries.  Just entier the directory names separated by spaces
# for example ARDUINO_LIBS = GSM EEPROM
#
ARDUINO_LIBS =
BOARD_TAG = uno
MONITOR_PORT = /dev/ttyUSB0

#
# additional libraries relative to your source
#
USER_LIB_PATH :=




