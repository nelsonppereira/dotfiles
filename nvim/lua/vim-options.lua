vim.opt.guicursor = ""

vim.opt.number = true
vim.opt.relativenumber = true

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.smartindent = true

vim.opt.wrap = true

vim.opt.swapfile = false
vim.opt.backup = false

vim.opt.hlsearch = false
vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true

vim.opt.termguicolors = true

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.colorcolumn = "80"
vim.opt.foldcolumn = "1"

vim.opt.wildchar = ('\t'):byte() -- '<Tab>'
vim.opt.wildmode = "list:longest"
vim.opt.wildmenu = true
vim.opt.magic = true
vim.opt.completeopt = {"menuone", "longest", "preview"}
vim.opt.complete = {".", "b"}

-- Some Key Bindings too 
--
--
vim.g.mapleader = " "
vim.keymap.set("n", "<leader>~", vim.cmd.NvimTreeToggle)
vim.keymap.set("n", "<leader>~~", "<cmd>Oil<CR>")
vim.keymap.set("n", "<leader>~~~", "<cmd>Oil ~/.config/nvim/lua/.<CR>")

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv") -- move visual block
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

vim.keymap.set({"n", "v"}, "<leader>y", [["+y]])
vim.keymap.set({"n", "v"}, "<leader>p", [["+p]])

-- This is going to get me cancelled
vim.keymap.set("i", "<C-c>", "<Esc>")

vim.keymap.set("n", "Q", "<nop>")
vim.keymap.set("n", "<leader>k", "<cmd>cnext<CR>zz")
vim.keymap.set("n", "<leader>j", "<cmd>cprev<CR>zz")

vim.keymap.set("n", "<leader>s", [[:%s/<C-r><C-w>/<C-r><C-w>/gI<Left><Left><Left>]])

vim.keymap.set("n", "<leader>n", "<cmd>set number! relativenumber!<CR>")
vim.api.nvim_create_user_command("LCD", 'lcd %:p:h', {})

vim.g.have_nerd_font = true
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1


