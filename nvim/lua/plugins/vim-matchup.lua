return {
  "andymass/vim-matchup",
  config = function()
    vim.g.match_matchparen_offscreen = { method = "popup" }
  end
}
