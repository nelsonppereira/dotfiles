return {
  -- {'Mofiqul/vscode.nvim',
  --
  --   enable=false,
  --
  --   lazy = false,
  --   priority = 1000, -- Colorschemes need high prioirty
  --   config = function()
  --     local c = require('vscode.colors').get_colors()
  --     require('vscode').setup({
  --       style = 'dark',
  --       transparent = true,
  --       italic_comments = true,
  --       underline_links = true,
  --       disable_nvimtree_bg = true,
  --       group_overrides = {
  --         Cursor = { fg=c.vscDarkBlue, bg=c.vscLightGreen, bold=true },
  --       }
  --     })
  --     require('vscode').load()
  --   end,
  -- },
  {
    "catppuccin/nvim",
    lazy=false,
    name = "catppuccin",
    priority = 1000,
    config = function()
      require("catppuccin").setup({
        transparent_background=true
      })
      vim.cmd.colorscheme  "catppuccin"
    end,
  }
}
