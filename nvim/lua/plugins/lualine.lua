return {
	"nvim-lualine/lualine.nvim",
	config = function()
		require("lualine").setup({
			options = { theme = "OceanicNext", icons_enabled = false },
			sections = {
        lualine_c = {
          { "filename", path = 0, shorting_target=80, file_status = true }
        },
      },
		})
	end,
}
