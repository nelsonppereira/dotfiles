return {
  "nvimtools/none-ls.nvim",
  config = function()
    local null_ls = require("null-ls")
    null_ls.setup({
      sources = {
        null_ls.builtins.formatting.gersemi,
        null_ls.builtins.formatting.stylua,
        null_ls.builtins.formatting.yapf.with({
          filetypes = {"python"},
        }),
        null_ls.builtins.formatting.biome.with({
          filetypes = {"javascript", "typescript", "json"},
        }),
        null_ls.builtins.formatting.prettierd.with({
          disabled_filetypes = {"javascript", "typescript", "json"},
        }),
      },
    })
  end,
}
