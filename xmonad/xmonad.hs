-- Version 0.15 --

import XMonad
import XMonad.StackSet (sink)
import XMonad.Layout.Named (named)
import XMonad.Layout.NoBorders (smartBorders)
import XMonad.Layout.Reflect (reflectHoriz)
import XMonad.Layout.IM (withIM, Property(..))
import XMonad.Layout.ToggleLayouts (toggleLayouts, ToggleLayout(..))
import XMonad.Layout.Tabbed (tabbedBottom, defaultTheme, activeColor, inactiveColor,
  urgentColor, activeTextColor, inactiveTextColor, urgentTextColor, activeBorderColor,
  inactiveBorderColor, urgentBorderColor, shrinkText)
import XMonad.Layout.ResizableTile 
import XMonad.Layout.TwoPane
import XMonad.Layout.ThreeColumns
import XMonad.Actions.WindowGo (raiseMaybe)
import XMonad.Actions.CycleWS (nextWS, prevWS, toggleWS)
import XMonad.Hooks.ManageDocks (avoidStruts, manageDocks, docksEventHook)
import XMonad.Hooks.ManageHelpers (doCenterFloat)
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, xmobarPP, ppOutput, ppCurrent, ppVisible,
  ppTitle, ppLayout, xmobarColor, wrap, shorten)
import XMonad.Hooks.EwmhDesktops (ewmh, fullscreenEventHook)
import XMonad.Hooks.UrgencyHook (withUrgencyHook, NoUrgencyHook(..))
import XMonad.Util.Run (spawnPipe, safeSpawn, runInTerm)
import XMonad.Util.SpawnOnce (spawnOnce)

import Data.Map (union, fromList)
import Data.Char (toUpper)
import System.IO (hPutStrLn)

main = do
  xmobar <- spawnPipe "/bin/xmobar"
  xmonad $ withUrgencyHook NoUrgencyHook $ ewmh $ myConfig xmobar

myConfig logHandle = defaultConfig
  { terminal           = "/usr/bin/kitty"
  , modMask            = myModMask
  , focusedBorderColor = "red"
  , borderWidth        = 6
  , layoutHook         = myLayoutHook
  , handleEventHook    = handleEventHook defaultConfig <+> docksEventHook
  , workspaces         = myWorkspaces
  , startupHook        = myStartupHook
  , logHook            = myLogHook logHandle
  , keys               = \c ->
                         myKeys `union` keys defaultConfig c
  , mouseBindings      = \c ->
                         myMouseBindings `union` mouseBindings defaultConfig c
  , focusFollowsMouse  = False
  , clickJustFocuses   = True
  }


myModMask = mod1Mask

myStartupHook :: X ()
myStartupHook = do
  spawnOnce "/bin/feh --bg-scale ~/.xmonad/anime1dark.png"
  spawnOnce "/bin/xcompmgr -C" -- enable transparency

myWorkspaces = map show [1..9]

myLogHook proc = dynamicLogWithPP $ xmobarPP
  { ppOutput  = hPutStrLn proc
  , ppCurrent = currentStyle
  , ppVisible = visibleStyle
  , ppTitle   = titleStyle
  , ppLayout  = (\layout -> case layout of
      "ResizableTall" -> "[#] "
      "TwoPane"       -> "[|] "
      "ThreeCol"      -> "[||]"
      "Tabbed"        -> "[_] "
      )
  }
  where
    currentStyle = xmobarColor "yellow" "" . wrap "[" "]"
    visibleStyle = wrap "(" ")"
    titleStyle   = xmobarColor "cyan" "" . shorten 80 . filterCurly
    filterCurly  = filter (not . isCurly)
    isCurly x    = x == '{' || x == '}'

myLayoutHook =
  avoidStruts $
  smartBorders $
  toggleLayouts tabbedLayout
  (resizable ||| twoPane ||| threeCol ||| tabbedLayout)
  where
   -- tiled     = Tall nmaster delta ratio
    resizable = named "ResizableTall" (reflectHoriz(ResizableTall nmaster delta ratio []))
    twoPane   = named "TwoPane" (reflectHoriz(TwoPane delta ratio))
    threeCol  = named "ThreeCol" (reflectHoriz(ThreeCol nmaster delta ratio))
    nmaster   = 1
    delta     = 3/100
    ratio     = 1/2
    
    tabbedLayout = named "Tabbed" (tabbedBottom shrinkText myTheme)

myKeys = fromList
  [ ((myModMask,   xK_p),      dmenu)
  , ((0,  xK_Print),       safeSpawn "gnome-screenshot" ["-i"])
  , ((myModMask,   xK_f),      sendMessage ToggleLayout)
  , ((myModMask,   xK_l),      sendMessage Shrink)
  , ((myModMask,   xK_h),      sendMessage Expand)
  , ((myModMask,   xK_F3),  safeSpawn "amixer" ["-c", "0", "set", "Master", "1+", "unmute"])
  , ((myModMask,   xK_F2),  safeSpawn "amixer" ["-c", "0", "set", "Master", "1-"])
  , ((myModMask .|. shiftMask, xK_h),  sendMessage MirrorShrink)
  , ((myModMask .|. shiftMask, xK_l),  sendMessage MirrorExpand)
  , ((myModMask .|. shiftMask, xK_Left),  prevWS)
  , ((myModMask .|. shiftMask, xK_Right), nextWS)
  , ((myModMask .|. shiftMask, xK_End),   safeSpawn "slock" [])

  -- force a new session
  , ((myModMask .|. shiftMask, xK_n), runInTerm "" "tmux new -A -s main")
  , ((myModMask .|. shiftMask, xK_t), runInTermOrRaise "tmux" "tmux new -A -s main")
  , ((myModMask .|. shiftMask, xK_f), spawnOrRaise "firefox")
  ]
  where
    spawnOrRaise p = raiseMaybe (safeSpawn p []) (className =? (upper p))
      where
        upper [] = ""
        upper (x:xs) = toUpper x : xs

    runInTermOrRaise t p = raiseMaybe (runInTerm ("--title " ++ t) p) (title =? t)

    dmenu = safeSpawn "dmenu_run" dmenuConfig
    dmenuConfig =
      ["-nb", "black", "-nf", "grey", "-sb", "magenta", "-sf", "white"
      , "-fn", "\"-misc-fixed-*-*-*-*-10-*-*-*-*-*-*-*\""
      ]

myMouseBindings = fromList
  [ ((0, 10),        \w -> toggleWS)
  , ((myModMask, 5), \w -> prevWS)
  , ((myModMask, 4), \w -> nextWS)
  ]


myTheme = defaultTheme
  { activeColor         = "#1a1a1a"
  , inactiveColor       = "#000000"
  , urgentColor         = "#1a1a1a"
  , activeTextColor     = "#00ffff"
  , inactiveTextColor   = "#ffbe33"
  , urgentTextColor     = "#ff00ff"
  , activeBorderColor   = "#000000"
  , inactiveBorderColor = "#1a1a1a"
  , urgentBorderColor   = "#000000"
  }


