"             _
"      _   __(_)___ ___  __________
"     | | / / / __ `__ \/ ___/ ___/
"    _| |/ / / / / / / / /  / /__
"   (_)___/_/_/ /_/ /_/_/   \___/
"
"

" ===================================================================
" {{{ Plugins
" ===================================================================
"
call plug#begin('~/.vim/plugged')
  Plug 'junegunn/vim-plug'
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
  Plug 'junegunn/fzf.vim'
  Plug 'itchyny/lightline.vim'
  Plug 'markonm/traces.vim'
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'neoclide/vim-jsx-improve'
  Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
  Plug 'tpope/vim-fugitive'
  Plug 'tpope/vim-commentary'
  Plug 'tpope/vim-surround'
  Plug 'mattn/emmet-vim'
call plug#end()

packadd! matchit

filetype plugin indent on

" Emmet
let g:user_emmet_install_global = 0
autocmd FileType html,htmldjango,django,javascript,javascriptreact,css EmmetInstall

au BufNewFile,BufRead *.js set filetype=javascriptreact

" FZF
"
let g:fzf_buffers_jump=1
command! -bang -nargs=* GGrep
  \ call fzf#vim#grep(
  \   'git grep --line-number -- '.shellescape(<q-args>), 0,
  \   fzf#vim#with_preview({'dir': systemlist('git rev-parse --show-toplevel')[0]}), <bang>0)

let g:fzf_layout = {'down' : '~40$' }



" lightline
"
let g:lightline = {
        \ 'colorscheme': 'powerline',
        \  }



"
" coc
"
set cmdheight=1
set updatetime=300

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif


"Map <tab> for trigger completion, completion confirm, snippet expand and jump
"like VSCode: requires coc-snippets extension>

  inoremap <silent><expr> <TAB>
    \ coc#pum#visible() ? coc#_select_confirm() :
    \ coc#expandableOrJumpable() ?
    \ "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
    \ CheckBackspace() ? "\<TAB>" :
    \ coc#refresh()

  function! CheckBackspace() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
  endfunction

  let g:coc_snippet_next = '<tab>'



"
"
" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" " Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>
"
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')


imap <C-l> <Plug>(coc-snippets-expand)
vmap <C-j> <Plug>(coc-snippets-select)
let g:coc_snippet_next ='<c-j>'
let g_coc_snippet_prev ='<c-k>'
imap <C-j> <Plug>(coc-snippets-expand-jump))
xmap <leader>x <Plug>(coc-convert-snippet)
"
" }}}

set background=dark
colorscheme vscode
syntax on


" ===================================================================
" {{{ Settings
" ===================================================================
set nocompatible
set modeline
set modelines=1
set laststatus=2
set shortmess=atc
set showcmd
set noshowmode
set matchpairs=(:),{:},[:]
set showmatch

"set foldmethod=indent
"set foldlevelstart=99
"set fillchars=vert:\|,fold:\
set foldcolumn=1
set ruler
set report=0

set number
set relativenumber

set notitle
set visualbell
set t_vb=

set wildchar=<TAB>
set wildmode=list:longest
set wildmenu

set ignorecase
set incsearch
set hlsearch
set smartcase
set magic

set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set backspace=indent,eol,start
set noerrorbells

set noswapfile
set ttimeout ttimeoutlen=50
set autowrite
set nobackup
set nowritebackup
set hidden

set completeopt=menuone,longest,preview
set complete=.,b

set lazyredraw
set textwidth=0

set viminfo=%,'50,\"100,:100,n~/.viminfo

" }}}


" ===================================================================
" {{{ Remappings
" ===================================================================
"

" Emacs Style commandline line editing
cnoremap <C-A> <Home>
cnoremap <C-E> <End>
cnoremap <C-F> <Right>
cnoremap <C-B> <Left>

" Completion for  tabs, Files, definitions
inoremap ^] ^X^]
inoremap ^F ^X^F
inoremap ^D ^X^D


" FZF
nnoremap <silent> <C-F>f :Files<CR>
nnoremap <silent> <C-F>g :GFiles<CR>

" }}}


nnoremap <Leader>,pyg :0r ~/.vim/snippets/pygame<CR>gg
nnoremap <Leader>,ino :0r ~/.vim/snippets/arduino.ino<CR>gg
nnoremap <Leader>,inomk :0r ~/.vim/snippets/arduino.mk<CR>gg

iabbr forl for (int i=0; i <; i++)<CR>{<CR><CR>}<esc>3k2f;i

"
"Some AutoCommands
"

function! StripTrailingWS()
    let _save_pos=getpos(".")
    %s/\s\+$//e
    let @/=_s
    nohl
    unlet _s
    call setpos('.', _save_pos)
    unlet _save_pos
endfunction

" chdir to current directory as current file
augroup vimrc
    autocmd!
    au BufEnter * silent! lcd %:p:h
    au BufWrite *.py silent! call StripTrailingWS()
augroup END
"

function! ToggleHeader()
    let filename = expand("%:p:r")
    let ext = expand("%:e")
    if ext =~ 'c'  || ext =~ 'cpp'
        if filereadable(filename.".h")
            execute "e %:r.h"
        elseif filereadable(filename.".H")
            execute "e %:r.H"
        elseif filereadable(filename.".hpp")
            execute "e %:r.hpp"
        endif
    elseif ext =~ 'h'  || ext =~ 'hpp'
        if filereadable(filename.".c")
            execute "e %:r.c"
        elseif filereadable(filename.".C")
            execute "e %:r.C"
        elseif filereadable(filename.".cpp")
            execute "e %:r.cpp"
        endif
    endif
endfunction
nnoremap ,o :call ToggleHeader()<cr>

command LCD lcd %:p:h
command TrimWhiteSpace %s/\s\+$//e

" ===================================================================
" {{{ Notes
" ===================================================================
"
" Bufdo apply the following search and replace to all buffers!

" :bufdo %s/pattern/replace/ge | update
"   Where,
"    | - separator between commands
"    update (write file if changes were made)


"  To see all keybindings
"    : help index


"  To see all mappings
"   :map or :nmap or :imap or :vmap

" }}}


" vim: ts=1 sw=2 et foldmethod=marker : foldlevel=1
